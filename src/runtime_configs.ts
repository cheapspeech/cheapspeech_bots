import fs from "fs/promises"

export default class RuntimeConfigs {
    static cassandraAddresses: Array<string> = []

    static async initialize() {
        try {
            var configFile = await fs.readFile("./conf/app_config.json")
        }
        catch (err) {
            throw new Error("Could not read config file ./conf/app_config.json")

        }
        var configFileJson = JSON.parse(configFile.toString())

        if (!configFileJson.hasOwnProperty("cassandraAddresses")) {
            throw new Error("app_config.json missing cassandraAddresses: Array<string> key.")
        }

        this.cassandraAddresses = configFileJson.cassandraAddresses
    }
}