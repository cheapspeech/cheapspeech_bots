import { Client, types } from "cassandra-driver";
import RuntimeConfigs from "../runtime_configs";
import { AvlTree, AvlTreeNode } from "datastructures-js"
import { exit } from "process";

const totalTrendingPosts = 10000

//TODO: Re-implement remark retrieval using streams
//TODO: Write unit tests for this one maybe...?

class CreateTrending {
    trendingTracker: AvlTree<number, Remark> = new AvlTree()
    count: number = 0

    async create() {
        await RuntimeConfigs.initialize()
        var con = await this.openCassandraConnection()
        this.loopCurrentRemarks(con)
    }

    async openCassandraConnection(): Promise<Client> {
        var client = new Client({
            keyspace: "cheapspeech",
            localDataCenter: "datacenter1",
            contactPoints: RuntimeConfigs.cassandraAddresses
        })
        await client.connect()
        return client;
    }

    loopCurrentRemarks(con: Client) {
        var selfRef = this
        con.eachRow("select * from remarks", [], {},
            (num, row) => {
                var score = this.calculateScore(row)
                this.insertNewNode(score, row.remarkid)
                this.removeSmallestNode()
            },
            (err: Error, result: types.ResultSet) => {
                selfRef.writeTrending(con)
            }
        )
    }

    calculateScore(row: types.Row) {
        var upvotes = row.upvotes ? row.upvotes : 0
        var downvotes = row.downvotes ? row.downvotes : 0

        var sum = upvotes - downvotes

        return sum
    }

    insertNewNode(score: number, remarkId: string) {
        var remarkNode = this.trendingTracker.find(score)
        if (remarkNode) {
            remarkNode.getValue().addRemark(remarkId)
        }
        else {
            this.trendingTracker.insert(score, new Remark(score, remarkId))
        }

        this.count++
    }

    removeSmallestNode() {
        if (this.count < totalTrendingPosts) {
            return this.count;
        }
        else {
            var lowest = this.trendingTracker.min()
            var val = lowest.getValue()
            var key = lowest.getKey()
            if (val.remarkIds.length > 1) {
                val.remarkIds.pop()
                this.count--
            }
            else {
                this.trendingTracker.remove(key)
                this.count--
            }
        }
    }

    async writeTrending(con: Client) {
        await con.execute("truncate trending");
        this.trendingTracker.traverseInOrder((node: AvlTreeNode<number, Remark>) => {
            var value = node.getValue()
            for (var remark of value.remarkIds) {
                con.execute("insert into trending (position, remarkid) values (?, ?)",
                    [
                        this.count,
                        remark
                    ],
                    {
                        hints: [
                            "varint",
                            "text"
                        ]
                    }
                )
                this.count--
                console.log("Inserting: " + remark + " at position: " + this.count.toString())
                if (this.count == 0) {
                    console.log("done")
                }
            }
        })
    }
}

class Remark {
    score: number
    remarkIds: Array<string>

    constructor(score: number, remarkId: string) {
        this.score = score
        this.remarkIds = [remarkId]
    }

    addRemark(remarkId: string) {
        this.remarkIds.push(remarkId)
    }

    removeRemark() {
        this.remarkIds.pop()
    }
}

function main() {
    var create = new CreateTrending()
    create.create()
}

main();